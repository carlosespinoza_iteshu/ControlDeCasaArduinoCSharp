int ledRojo=5;
int ledVerde=4;
int ledAmarillo=3;

bool estadoRojo=true;
bool estadoVerde=true;
bool estadoAmarillo=true;

void setup() {
 pinMode(ledRojo,OUTPUT);
 pinMode(ledVerde,OUTPUT);
 pinMode(ledAmarillo,OUTPUT);
 pinMode(A0,INPUT);
 Serial.begin(9600);
 delay(2000);
 Serial.println("Encendido leds");
 digitalWrite(ledRojo,HIGH);
 digitalWrite(ledAmarillo,HIGH);
 digitalWrite(ledVerde,HIGH);
}

void loop() {
  Serial.print("#");
  Serial.println(analogRead(A0));
  delay(500);
}
//Se ejecuta cada vez que el arduino detecta datos de entrada por el puerto serie.
void serialEvent(){
  while(Serial.available()){
    char comando=(char)Serial.read();
    switch(comando){
      case 'r':
        if(estadoRojo){
          digitalWrite(ledRojo,LOW);
          estadoRojo=false;
          Serial.println("Led Rojo Apagado");
        }else{
          digitalWrite(ledRojo,HIGH);
          estadoRojo=true;
          Serial.println("Led Rojo Encendido");
        }
      break;
      case 'v':
        if(estadoVerde){
          digitalWrite(ledVerde,LOW);
          estadoVerde=false;
          Serial.println("Led Verde Apagado");
        }else{
          digitalWrite(ledVerde,HIGH);
          estadoVerde=true;
          Serial.println("Led Verde Encendido");
        }
      break;
      case 'a':
        if(estadoAmarillo){
          digitalWrite(ledAmarillo,LOW);
          estadoAmarillo=false;
          Serial.println("Led Amarillo Apagado");
        }else{
          digitalWrite(ledAmarillo,HIGH);
          estadoAmarillo=true;
          Serial.println("Led Amarillo Encendido");
        }
      break;
      default:
        Serial.println("Comando no reconocido");
      break;
    }
  }
}

