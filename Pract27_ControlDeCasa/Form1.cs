﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pract27_ControlDeCasa
{
    public partial class Form1 : Form
    {
        PuertoSerie puerto;
        int valor = 0;
        string log = "";
        Timer timer;
        
        public Form1()
        {
            InitializeComponent();
            puerto = new Pract27_ControlDeCasa.PuertoSerie();
            puerto.Evento += Puerto_Evento;
            puerto.DatoObtenido += Puerto_DatoObtenido;
            cmbPuertos.DataSource = puerto.PuertosDisponibles();
            timer = new Timer();
            timer.Interval = 10;
            timer.Tick += Timer_Tick;
            
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            pgbTemperatura.Value = valor;
            if (log != "")
            {
                txtLog.Text = log + "\r\n" + txtLog.Text;
                log = "";
            }
        }

        private void Puerto_DatoObtenido(object sender, string e)
        {
            //#89
            if (e.Contains("#"))
            {
                //Dato correcto
                if (!int.TryParse(e.Replace("#", ""),out valor))
                {
                    log = "Error al procesar el dato: " + e;
                }
                else
                {
                    log = "Temperatura recibida: " + valor;
                }
            }
            else
            {
                log = "Mensaje Recibido: " + e;
            }

            
        }

        private void Puerto_Evento(object sender, EventArgs e)
        {
            log = "Evento : " + (string)sender;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void chkConectado_CheckedChanged(object sender, EventArgs e)
        {
            if (chkConectado.Checked)
            {
                //Tiene la palomita
                if (puerto.Conectar(cmbPuertos.Text))
                {
                    timer.Start();
                }


            }else
            {   //Quitar la palomita
                if (puerto.Desconectar())
                {
                    timer.Stop();
                }
            }
        }

        private void btnLedRojo_Click(object sender, EventArgs e)
        {
            puerto.Escribir("r");
        }

        private void btnLedVerde_Click(object sender, EventArgs e)
        {
            puerto.Escribir("v");
        }

        private void btnLedAmarillo_Click(object sender, EventArgs e)
        {
            puerto.Escribir("a");
        }
    }
}
