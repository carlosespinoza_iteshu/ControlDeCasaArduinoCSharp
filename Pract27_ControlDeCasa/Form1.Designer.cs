﻿namespace Pract27_ControlDeCasa
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnLedRojo = new System.Windows.Forms.Button();
            this.btnLedVerde = new System.Windows.Forms.Button();
            this.btnLedAmarillo = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.cmbPuertos = new System.Windows.Forms.ComboBox();
            this.chkConectado = new System.Windows.Forms.CheckBox();
            this.pgbTemperatura = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Puerto:";
            // 
            // btnLedRojo
            // 
            this.btnLedRojo.Location = new System.Drawing.Point(15, 44);
            this.btnLedRojo.Name = "btnLedRojo";
            this.btnLedRojo.Size = new System.Drawing.Size(75, 23);
            this.btnLedRojo.TabIndex = 1;
            this.btnLedRojo.Text = "Led Rojo";
            this.btnLedRojo.UseVisualStyleBackColor = true;
            this.btnLedRojo.Click += new System.EventHandler(this.btnLedRojo_Click);
            // 
            // btnLedVerde
            // 
            this.btnLedVerde.Location = new System.Drawing.Point(96, 44);
            this.btnLedVerde.Name = "btnLedVerde";
            this.btnLedVerde.Size = new System.Drawing.Size(75, 23);
            this.btnLedVerde.TabIndex = 2;
            this.btnLedVerde.Text = "Led Verde";
            this.btnLedVerde.UseVisualStyleBackColor = true;
            this.btnLedVerde.Click += new System.EventHandler(this.btnLedVerde_Click);
            // 
            // btnLedAmarillo
            // 
            this.btnLedAmarillo.Location = new System.Drawing.Point(177, 44);
            this.btnLedAmarillo.Name = "btnLedAmarillo";
            this.btnLedAmarillo.Size = new System.Drawing.Size(75, 23);
            this.btnLedAmarillo.TabIndex = 3;
            this.btnLedAmarillo.Text = "LedAmarillo";
            this.btnLedAmarillo.UseVisualStyleBackColor = true;
            this.btnLedAmarillo.Click += new System.EventHandler(this.btnLedAmarillo_Click);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(16, 122);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(235, 238);
            this.txtLog.TabIndex = 5;
            // 
            // cmbPuertos
            // 
            this.cmbPuertos.FormattingEnabled = true;
            this.cmbPuertos.Location = new System.Drawing.Point(65, 6);
            this.cmbPuertos.Name = "cmbPuertos";
            this.cmbPuertos.Size = new System.Drawing.Size(121, 21);
            this.cmbPuertos.TabIndex = 6;
            // 
            // chkConectado
            // 
            this.chkConectado.AutoSize = true;
            this.chkConectado.Location = new System.Drawing.Point(192, 8);
            this.chkConectado.Name = "chkConectado";
            this.chkConectado.Size = new System.Drawing.Size(69, 17);
            this.chkConectado.TabIndex = 7;
            this.chkConectado.Text = "Conectar";
            this.chkConectado.UseVisualStyleBackColor = true;
            this.chkConectado.CheckedChanged += new System.EventHandler(this.chkConectado_CheckedChanged);
            // 
            // pgbTemperatura
            // 
            this.pgbTemperatura.Location = new System.Drawing.Point(15, 95);
            this.pgbTemperatura.Maximum = 1023;
            this.pgbTemperatura.Name = "pgbTemperatura";
            this.pgbTemperatura.Size = new System.Drawing.Size(235, 21);
            this.pgbTemperatura.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Temperatura Actual";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 372);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pgbTemperatura);
            this.Controls.Add(this.chkConectado);
            this.Controls.Add(this.cmbPuertos);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.btnLedAmarillo);
            this.Controls.Add(this.btnLedVerde);
            this.Controls.Add(this.btnLedRojo);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Control de Casa";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLedRojo;
        private System.Windows.Forms.Button btnLedVerde;
        private System.Windows.Forms.Button btnLedAmarillo;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.ComboBox cmbPuertos;
        private System.Windows.Forms.CheckBox chkConectado;
        private System.Windows.Forms.ProgressBar pgbTemperatura;
        private System.Windows.Forms.Label label2;
    }
}

